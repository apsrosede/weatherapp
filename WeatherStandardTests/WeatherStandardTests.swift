//
//  WeatherStandardTests.swift
//  WeatherStandardTests
//
//  Created by AdrianRoseMacBookAir on 12.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import XCTest
@testable import WeatherStandard

class WeatherStandardTests: XCTestCase {

    // MARK: Properties needed for Testing Purposes
    let cityOne = "Munich"
    lazy var urlStringMunich: String = {
        return      "https://api.openweathermap.org/data/2.5/forecast?q=\(self.cityOne)&appid=822b4b31eb8468e6221ccd8e9623d3dd"
        }()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Unit Tests below
    // MARK: Is the Network working Test ?
    func testNetworkCallResultIs200() {
        let theExpectation = expectation(description: "firstNetworkCall")
        
       
        let urlMUC = URL(string: urlStringMunich)
            
            let task = URLSession.shared.weatherTask(with:urlMUC! ) { (weatherReturned, response, error) in
                
                if let resultCode = (response as? HTTPURLResponse)?.statusCode, resultCode == 200 {
                    
                    XCTAssertEqual(resultCode, 200, " Response status code should be 200")
                } else {
                    XCTFail("Response status code was not 200")
                }
                  theExpectation.fulfill()
            }
            task.resume()
        
         waitForExpectations(timeout: 10) { (error) in
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task.cancel()

        }
    }

    // MARK: How Fast Does the Network call take and Json Parsing to Codable Data Type ?
    
    func testHowLongToGetAndParseAWeatherFileForCityPerformance() {
        
         let theSecondExpectation = expectation(description: "SecondtNetworkCall")
         
            var task: URLSessionDataTask?
        
            let urlMUC = URL(string: urlStringMunich)
        
             task = URLSession.shared.weatherTask(with:urlMUC! ) { (weatherReturned, response, error) in
                
                guard let weatherObject = weatherReturned else {
                    return
                }
                print("Weather object is: \(String(describing: weatherObject.city))")
                theSecondExpectation.fulfill()
            }

             self.measure {
                    task?.resume()
             }

            waitForExpectations(timeout: 10) { (error) in
            
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task?.cancel()

        }

        
        
        
    }

}
