//
//  Networking.swift
//  WeatherStandard
//
//  Created by AdrianRoseMacBookAir on 12.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import Foundation

// MARK : Public Types

enum networkCallResults {
    case ok(Weather)
    case error(Error)
    case noData
    case responseError(Int)
}


// MARK : Variables & Constants

let appID = "822b4b31eb8468e6221ccd8e9623d3dd"
var cityID = "524901" // London
var cityName = "München" // Munich
var lat = 123
var lon = 123

let localFileName = "weather.bin"

let callByCityStringExample = "https://samples.openweathermap.org/data/2.5/forecast?q=München,DE&appid=b6907d289e10d714a6e88b30761fae22"
let callByCityString = "https://api.openweathermap.org/data/2.5/forecast?q=Munich&appid=822b4b31eb8468e6221ccd8e9623d3dd"
let callByIdString = "https://api.openweathermap.org/data/2.5/forecast?id=524901&appid=822b4b31eb8468e6221ccd8e9623d3dd"
let callByLatLon = "https://api.openweathermap.org/data/2.5/forecast?lat=\(lat)&lon=\(lon)&appid=822b4b31eb8468e6221ccd8e9623d3dd"

// MARK: - Weather
struct Weather: Codable {
    let cod: String?
    let message: Double? // Can be String ?
    let cnt: Int?
    let list: [List]?
    let city: City?
}


// MARK: - City
struct City: Codable {
    let id: Int?
    let name: String?
    let coord: Coord?
    let country: String?
    let population, timezone, sunrise, sunset: Int?
}


// MARK: - Coord
struct Coord: Codable {
    let lat, lon: Double?
}


// MARK: - List
struct List: Codable {
    let dt: Int?
    let main: MainClass?
    let weather: [WeatherElement]?
    let clouds: Clouds?
    let wind: Wind?
    let sys: Sys?
    let dtTxt: String?
    let rain: Rain?
    
    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, sys
        case dtTxt = "dt_txt"
        case rain
    }
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Int?
}

// MARK: - MainClass
struct MainClass: Codable {
    let temp, tempMin, tempMax, pressure: Double?
    let seaLevel, grndLevel: Double?
    let humidity: Int?
    let tempKf: Double?
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}


// MARK: - Rain
struct Rain: Codable {
    let the3H: Double?
    
    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}


// MARK: - Sys
struct Sys: Codable {
    let pod: String?
}

enum Pod: String, Codable {
    case d = "d"
    case n = "n"
}


// MARK: - WeatherElement
struct WeatherElement: Codable {
    let id: Int?
    let main: String?
    let weatherDescription: String? //Description?
    let icon: String?
    
    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
}

enum Description: String, Codable {
    case brokenClouds = "broken clouds"
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case lightRain = "light rain"
    case overcastClouds = "overcast clouds"
    case scatteredClouds = "scattered clouds"
}

// MARK: - Wind
struct Wind: Codable {
    let speed, deg: Double?
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
    return decoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            
            do {
                let decoder = newJSONDecoder()
                let result = try decoder.decode(T.self, from: data)
                let savedData = self.saveDataObjectIfValidJson(dataToSave: data)
                print("local copy of data: \(savedData)")
                completionHandler(result, response, nil)
            }
                catch(let theError) {
                    print("Error is : \(theError)")
                    completionHandler(nil, response, theError)
                }
        }
    }
    
     func lookForSavedDataFileAndLoad(completionHandler: (_ callSuccess: networkCallResults ) -> ()) -> Void {
        
        let fileManager = FileManager.default
        let validData: Data?
        
        do {
            if let documentsPathURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                let URLToFile = documentsPathURL.appendingPathComponent(localFileName)
                if fileManager.fileExists(atPath: URLToFile.path) {
                    validData = try Data(contentsOf: URLToFile)
                    guard let validData = validData else {
                        return
                    }
                    print("Retrieved Data at : \(URLToFile.path)")
                    let decoder = newJSONDecoder()
                    let result = try decoder.decode(Weather.self, from: validData)
                    completionHandler(.ok(result))
                }
            }

        } catch(let theError)
        {
            print("Unable to find data file or Parse: \(theError)")
            completionHandler(.error(theError))
        }

        
        
    }
    
    private func saveDataObjectIfValidJson(dataToSave validData: Data) -> Bool {
        let fileManager = FileManager.default
        var success = true
        
        do {
            if let documentsPathURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                let URLToFile = documentsPathURL.appendingPathComponent(localFileName)
                if fileManager.fileExists(atPath: URLToFile.path) {
                    try fileManager.removeItem(at: URL(fileURLWithPath: URLToFile.path))
                }
                let _ = try validData.write(to: URLToFile, options: .atomic)
                print("Saving Data at : \(URLToFile.path)")
            }

        } catch(let theError)
        {
            success = false
            print("\(#function) unable to save or delete data \(theError)")
        }
            
        return success
    }
    
    func weatherTask(with url: URL, completionHandler: @escaping (Weather?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    
}

    


