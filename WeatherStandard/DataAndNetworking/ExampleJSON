// 5 day Forecast Moscow

Parameters:

code Internal parameter
message Internal parameter
city
city.id City ID
city.name City name
city.coord
city.coord.lat City geo location, latitude
city.coord.lon City geo location, longitude
city.country Country code (GB, JP etc.)
city.timezone Shift in seconds from UTC
cnt Number of lines returned by this API call
list
list.dt Time of data forecasted, unix, UTC
list.main
list.main.temp Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
list.main.temp_min Minimum temperature at the moment of calculation. This is deviation from 'temp' that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
list.main.temp_max Maximum temperature at the moment of calculation. This is deviation from 'temp' that is possible for large cities and megalopolises geographically expanded (use these parameter optionally). Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
list.main.pressure Atmospheric pressure on the sea level by default, hPa
list.main.sea_level Atmospheric pressure on the sea level, hPa
list.main.grnd_level Atmospheric pressure on the ground level, hPa
list.main.humidity Humidity, %
list.main.temp_kf Internal parameter
list.weather (more info Weather condition codes)
list.weather.id Weather condition id
list.weather.main Group of weather parameters (Rain, Snow, Extreme etc.)
list.weather.description Weather condition within the group
list.weather.icon Weather icon id
list.clouds
list.clouds.all Cloudiness, %
list.wind
list.wind.speed Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
list.wind.deg Wind direction, degrees (meteorological)
list.rain
list.rain.3h Rain volume for last 3 hours, mm
list.snow
list.snow.3h Snow volume for last 3 hours
list.dt_txt Data/time of calculation, UTC

{
    "cod": "200",
    "message": 0.0078,
    "cnt": 40,
    "list": [
        {
            "dt": 1570892400, - Time of data forecasted, unix, UTC
            "main": {
                "temp": 284.53, - Unit Default: Kelvin
                "temp_min": 283.525,
                "temp_max": 284.53,
                "pressure": 1003.71,
                "sea_level": 1003.71,
                "grnd_level": 981.62,
                "humidity": 78 , - %
                "temp_kf": 1
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain", -
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100 Cloudiness, %
            },
            "wind": {
                "speed": 8.39, - Unit Default: meter/sec,
                "deg": 228.808
            },
            "rain": {
                "3h": 0.312 - Rain volume for last 3 hours, mm
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-12 15:00:00" - Data/time of calculation, UTC
        },
        {
            "dt": 1570903200,
            "main": {
                "temp": 284.14,
                "temp_min": 283.393,
                "temp_max": 284.14,
                "pressure": 1003.02,
                "sea_level": 1003.02,
                "grnd_level": 980.72,
                "humidity": 85,
                "temp_kf": 0.75
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 8.11,
                "deg": 237.41
            },
            "rain": {
                "3h": 0.75
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-12 18:00:00"
        },
        {
            "dt": 1570914000,
            "main": {
                "temp": 283.46,
                "temp_min": 282.961,
                "temp_max": 283.46,
                "pressure": 1002.91,
                "sea_level": 1002.91,
                "grnd_level": 980.82,
                "humidity": 83,
                "temp_kf": 0.5
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.05,
                "deg": 286.342
            },
            "rain": {
                "3h": 2.875
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-12 21:00:00"
        },
        {
            "dt": 1570924800,
            "main": {
                "temp": 281.97,
                "temp_min": 281.72,
                "temp_max": 281.97,
                "pressure": 1004.52,
                "sea_level": 1004.52,
                "grnd_level": 982.23,
                "humidity": 88,
                "temp_kf": 0.25
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.12,
                "deg": 261.119
            },
            "rain": {
                "3h": 0.063
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-13 00:00:00"
        },
        {
            "dt": 1570935600,
            "main": {
                "temp": 281.149,
                "temp_min": 281.149,
                "temp_max": 281.149,
                "pressure": 1005.67,
                "sea_level": 1005.67,
                "grnd_level": 983.32,
                "humidity": 86,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.45,
                "deg": 263.818
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-13 03:00:00"
        },
        {
            "dt": 1570946400,
            "main": {
                "temp": 282.202,
                "temp_min": 282.202,
                "temp_max": 282.202,
                "pressure": 1006.94,
                "sea_level": 1006.94,
                "grnd_level": 984.52,
                "humidity": 80,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 6.02,
                "deg": 267.431
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-13 06:00:00"
        },
        {
            "dt": 1570957200,
            "main": {
                "temp": 282.862,
                "temp_min": 282.862,
                "temp_max": 282.862,
                "pressure": 1008.23,
                "sea_level": 1008.23,
                "grnd_level": 986,
                "humidity": 74,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 6.42,
                "deg": 267.5
            },
            "rain": {
                "3h": 0.062
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-13 09:00:00"
        },
        {
            "dt": 1570968000,
            "main": {
                "temp": 283.558,
                "temp_min": 283.558,
                "temp_max": 283.558,
                "pressure": 1008.91,
                "sea_level": 1008.91,
                "grnd_level": 986.56,
                "humidity": 73,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.64,
                "deg": 262.107
            },
            "rain": {
                "3h": 0.063
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-13 12:00:00"
        },
        {
            "dt": 1570978800,
            "main": {
                "temp": 282.349,
                "temp_min": 282.349,
                "temp_max": 282.349,
                "pressure": 1010.28,
                "sea_level": 1010.28,
                "grnd_level": 987.74,
                "humidity": 83,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 45
            },
            "wind": {
                "speed": 4.79,
                "deg": 259.105
            },
            "rain": {
                "3h": 0.062
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-13 15:00:00"
        },
        {
            "dt": 1570989600,
            "main": {
                "temp": 281.795,
                "temp_min": 281.795,
                "temp_max": 281.795,
                "pressure": 1012.26,
                "sea_level": 1012.26,
                "grnd_level": 989.41,
                "humidity": 83,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "clouds": {
                "all": 34
            },
            "wind": {
                "speed": 5.43,
                "deg": 272.112
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-13 18:00:00"
        },
        {
            "dt": 1571000400,
            "main": {
                "temp": 280.946,
                "temp_min": 280.946,
                "temp_max": 280.946,
                "pressure": 1013.55,
                "sea_level": 1013.55,
                "grnd_level": 990.75,
                "humidity": 86,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 76
            },
            "wind": {
                "speed": 5.11,
                "deg": 274.636
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-13 21:00:00"
        },
        {
            "dt": 1571011200,
            "main": {
                "temp": 280.15,
                "temp_min": 280.15,
                "temp_max": 280.15,
                "pressure": 1015.06,
                "sea_level": 1015.06,
                "grnd_level": 992.15,
                "humidity": 87,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 73
            },
            "wind": {
                "speed": 5.13,
                "deg": 269.81
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-14 00:00:00"
        },
        {
            "dt": 1571022000,
            "main": {
                "temp": 279.381,
                "temp_min": 279.381,
                "temp_max": 279.381,
                "pressure": 1016.14,
                "sea_level": 1016.14,
                "grnd_level": 993.25,
                "humidity": 89,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.72,
                "deg": 277.778
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-14 03:00:00"
        },
        {
            "dt": 1571032800,
            "main": {
                "temp": 280.676,
                "temp_min": 280.676,
                "temp_max": 280.676,
                "pressure": 1018.07,
                "sea_level": 1018.07,
                "grnd_level": 995.1,
                "humidity": 78,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.12,
                "deg": 287.5
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-14 06:00:00"
        },
        {
            "dt": 1571043600,
            "main": {
                "temp": 282.157,
                "temp_min": 282.157,
                "temp_max": 282.157,
                "pressure": 1018.99,
                "sea_level": 1018.99,
                "grnd_level": 996.04,
                "humidity": 73,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 58
            },
            "wind": {
                "speed": 4.42,
                "deg": 266.266
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-14 09:00:00"
        },
        {
            "dt": 1571054400,
            "main": {
                "temp": 283.359,
                "temp_min": 283.359,
                "temp_max": 283.359,
                "pressure": 1018.1,
                "sea_level": 1018.1,
                "grnd_level": 995.48,
                "humidity": 73,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 60
            },
            "wind": {
                "speed": 2.93,
                "deg": 261.627
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-14 12:00:00"
        },
        {
            "dt": 1571065200,
            "main": {
                "temp": 281.615,
                "temp_min": 281.615,
                "temp_max": 281.615,
                "pressure": 1015.9,
                "sea_level": 1015.9,
                "grnd_level": 993.13,
                "humidity": 89,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 2.26,
                "deg": 126.672
            },
            "rain": {
                "3h": 1.938
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-14 15:00:00"
        },
        {
            "dt": 1571076000,
            "main": {
                "temp": 282.107,
                "temp_min": 282.107,
                "temp_max": 282.107,
                "pressure": 1014.73,
                "sea_level": 1014.73,
                "grnd_level": 991.99,
                "humidity": 94,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.25,
                "deg": 145.728
            },
            "rain": {
                "3h": 2.562
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-14 18:00:00"
        },
        {
            "dt": 1571086800,
            "main": {
                "temp": 285.265,
                "temp_min": 285.265,
                "temp_max": 285.265,
                "pressure": 1012.7,
                "sea_level": 1012.7,
                "grnd_level": 990.15,
                "humidity": 92,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.28,
                "deg": 248.293
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-14 21:00:00"
        },
        {
            "dt": 1571097600,
            "main": {
                "temp": 285.024,
                "temp_min": 285.024,
                "temp_max": 285.024,
                "pressure": 1012.51,
                "sea_level": 1012.51,
                "grnd_level": 990.44,
                "humidity": 92,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.73,
                "deg": 258.165
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-15 00:00:00"
        },
        {
            "dt": 1571108400,
            "main": {
                "temp": 285.3,
                "temp_min": 285.3,
                "temp_max": 285.3,
                "pressure": 1013.12,
                "sea_level": 1013.12,
                "grnd_level": 990.86,
                "humidity": 92,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.19,
                "deg": 260.759
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-15 03:00:00"
        },
        {
            "dt": 1571119200,
            "main": {
                "temp": 285.703,
                "temp_min": 285.703,
                "temp_max": 285.703,
                "pressure": 1014.32,
                "sea_level": 1014.32,
                "grnd_level": 991.9,
                "humidity": 86,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.45,
                "deg": 270.599
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-15 06:00:00"
        },
        {
            "dt": 1571130000,
            "main": {
                "temp": 286.751,
                "temp_min": 286.751,
                "temp_max": 286.751,
                "pressure": 1015.71,
                "sea_level": 1015.71,
                "grnd_level": 993.11,
                "humidity": 73,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 98
            },
            "wind": {
                "speed": 5.5,
                "deg": 285.113
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-15 09:00:00"
        },
        {
            "dt": 1571140800,
            "main": {
                "temp": 286.847,
                "temp_min": 286.847,
                "temp_max": 286.847,
                "pressure": 1016.75,
                "sea_level": 1016.75,
                "grnd_level": 994.17,
                "humidity": 69,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 66
            },
            "wind": {
                "speed": 4.44,
                "deg": 298.601
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-15 12:00:00"
        },
        {
            "dt": 1571151600,
            "main": {
                "temp": 284.893,
                "temp_min": 284.893,
                "temp_max": 284.893,
                "pressure": 1017.7,
                "sea_level": 1017.7,
                "grnd_level": 995.15,
                "humidity": 78,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 70
            },
            "wind": {
                "speed": 2.21,
                "deg": 318.334
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-15 15:00:00"
        },
        {
            "dt": 1571162400,
            "main": {
                "temp": 282.969,
                "temp_min": 282.969,
                "temp_max": 282.969,
                "pressure": 1019.12,
                "sea_level": 1019.12,
                "grnd_level": 996.57,
                "humidity": 83,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 85
            },
            "wind": {
                "speed": 0.47,
                "deg": 135.261
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-15 18:00:00"
        },
        {
            "dt": 1571173200,
            "main": {
                "temp": 283.6,
                "temp_min": 283.6,
                "temp_max": 283.6,
                "pressure": 1019.31,
                "sea_level": 1019.31,
                "grnd_level": 996.79,
                "humidity": 84,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.94,
                "deg": 168.626
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-15 21:00:00"
        },
        {
            "dt": 1571184000,
            "main": {
                "temp": 283.244,
                "temp_min": 283.244,
                "temp_max": 283.244,
                "pressure": 1019.27,
                "sea_level": 1019.27,
                "grnd_level": 996.69,
                "humidity": 87,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 2.26,
                "deg": 169.754
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-16 00:00:00"
        },
        {
            "dt": 1571194800,
            "main": {
                "temp": 282.324,
                "temp_min": 282.324,
                "temp_max": 282.324,
                "pressure": 1018.83,
                "sea_level": 1018.83,
                "grnd_level": 996.21,
                "humidity": 89,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 1.85,
                "deg": 162.602
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-16 03:00:00"
        },
        {
            "dt": 1571205600,
            "main": {
                "temp": 283.7,
                "temp_min": 283.7,
                "temp_max": 283.7,
                "pressure": 1018.22,
                "sea_level": 1018.22,
                "grnd_level": 995.65,
                "humidity": 90,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 97
            },
            "wind": {
                "speed": 2.53,
                "deg": 149.566
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-16 06:00:00"
        },
        {
            "dt": 1571216400,
            "main": {
                "temp": 285.885,
                "temp_min": 285.885,
                "temp_max": 285.885,
                "pressure": 1017.4,
                "sea_level": 1017.4,
                "grnd_level": 994.95,
                "humidity": 85,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 2.53,
                "deg": 168.557
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-16 09:00:00"
        },
        {
            "dt": 1571227200,
            "main": {
                "temp": 287.248,
                "temp_min": 287.248,
                "temp_max": 287.248,
                "pressure": 1016.37,
                "sea_level": 1016.37,
                "grnd_level": 994,
                "humidity": 76,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.91,
                "deg": 186.754
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-16 12:00:00"
        },
        {
            "dt": 1571238000,
            "main": {
                "temp": 285.042,
                "temp_min": 285.042,
                "temp_max": 285.042,
                "pressure": 1015.94,
                "sea_level": 1015.94,
                "grnd_level": 993.39,
                "humidity": 88,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.03,
                "deg": 199.883
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-16 15:00:00"
        },
        {
            "dt": 1571248800,
            "main": {
                "temp": 284.2,
                "temp_min": 284.2,
                "temp_max": 284.2,
                "pressure": 1016.17,
                "sea_level": 1016.17,
                "grnd_level": 993.56,
                "humidity": 92,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 1.19,
                "deg": 231.843
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-16 18:00:00"
        },
        {
            "dt": 1571259600,
            "main": {
                "temp": 283.05,
                "temp_min": 283.05,
                "temp_max": 283.05,
                "pressure": 1015.57,
                "sea_level": 1015.57,
                "grnd_level": 992.91,
                "humidity": 94,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 93
            },
            "wind": {
                "speed": 1.4,
                "deg": 253.214
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-16 21:00:00"
        },
        {
            "dt": 1571270400,
            "main": {
                "temp": 282.256,
                "temp_min": 282.256,
                "temp_max": 282.256,
                "pressure": 1015.46,
                "sea_level": 1015.46,
                "grnd_level": 992.74,
                "humidity": 94,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 92
            },
            "wind": {
                "speed": 0.75,
                "deg": 257.804
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-17 00:00:00"
        },
        {
            "dt": 1571281200,
            "main": {
                "temp": 281.651,
                "temp_min": 281.651,
                "temp_max": 281.651,
                "pressure": 1015.17,
                "sea_level": 1015.17,
                "grnd_level": 992.48,
                "humidity": 95,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 0.23,
                "deg": 159.702
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2019-10-17 03:00:00"
        },
        {
            "dt": 1571292000,
            "main": {
                "temp": 282.686,
                "temp_min": 282.686,
                "temp_max": 282.686,
                "pressure": 1015.14,
                "sea_level": 1015.14,
                "grnd_level": 992.37,
                "humidity": 93,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 92
            },
            "wind": {
                "speed": 1.09,
                "deg": 86.794
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-17 06:00:00"
        },
        {
            "dt": 1571302800,
            "main": {
                "temp": 285.311,
                "temp_min": 285.311,
                "temp_max": 285.311,
                "pressure": 1015.83,
                "sea_level": 1015.83,
                "grnd_level": 993.26,
                "humidity": 84,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 77
            },
            "wind": {
                "speed": 1.81,
                "deg": 74.714
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-17 09:00:00"
        },
        {
            "dt": 1571313600,
            "main": {
                "temp": 284.79,
                "temp_min": 284.79,
                "temp_max": 284.79,
                "pressure": 1015.81,
                "sea_level": 1015.81,
                "grnd_level": 993.25,
                "humidity": 88,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 87
            },
            "wind": {
                "speed": 2.62,
                "deg": 61.32
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-10-17 12:00:00"
        }
    ],
    "city": {
        "id": 524901,
        "name": "Moscow",
        "coord": {
            "lat": 55.7522,
            "lon": 37.6156
        },
        "country": "RU",
        "timezone": 10800,
        "sunrise": 1570852381,
        "sunset": 1570891137
    }
}
