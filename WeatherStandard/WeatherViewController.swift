//
//  ViewController.swift
//  WeatherStandard
//
//  Created by AdrianRoseMacBookAir on 12.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class WeatherViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var theMapView: MKMapView!
    @IBOutlet weak var theCityNameField: UITextField!
    @IBOutlet weak var theTableView: UITableView!
    
    @IBOutlet weak var theSpinner: UIActivityIndicatorView!
    
    @IBAction func loadLocally(_ sender: UISwitch) {
        if sender.isOn == false {
            
            print("load data locally")
            URLSession.shared.lookForSavedDataFileAndLoad { (enumResult) in
                
                switch enumResult {
                case .ok(let theWeatherFound):
                    self.currentWeatherForCity = theWeatherFound
                case .error(let theError):
                    self.handleErrorWithAnAlert()
                    print("The error \(theError)")
                default:
                    print("Others")
                }

            }

        }
    }
    
    // MARK: Properties
    var location = CLLocationManager()
    var latestLocation: CLLocation?
    var firstLocation: CLLocation?
    
    var storedOffsets = [Int: CGFloat]()
    var currentWeatherForCity: Weather? {
        didSet {
            DispatchQueue.main.async {
                print("Updating Weather \(String(describing: self.currentWeatherForCity))")
                self.theTableView.reloadData()
                if let lat = self.currentWeatherForCity?.city?.coord?.lat, let long = self.currentWeatherForCity?.city?.coord?.lon {
                    self.latestLocation = CLLocation(latitude: lat, longitude: long)
                    self.centerMapOnLocation(location: CLLocation(latitude: lat, longitude: long))
                }
                self.stopSpinnerOnMain()
            }
        }
    }
    var theTimer: Timer?
    var theCurrentTimerCityString = ""
    let theNumberOfRowsInTableView = 5
    let theNumberOfCellsInHorizontalCollectionView = 8
    let latLongMunich = CLLocation(latitude: 48.1351, longitude: 11.5820)
    let regionRadius: CLLocationDistance = 2000
    let collectionViewColors = [UIColor.generateRandomPastelColor(withMixedColor: UIColor.yellow),UIColor.generateRandomPastelColor(withMixedColor: nil),UIColor.generateRandomPastelColor(withMixedColor: nil),UIColor.generateRandomPastelColor(withMixedColor: nil),UIColor.generateRandomPastelColor(withMixedColor: nil)]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: Start Setting Up

        setUpCoreLocation()
        
        centerMapOnLocation(location: latLongMunich)
    }
    
    func updateTheWeather(cityStringURLToRetrieve city: String) {
        
        // MARK: SetUp Timer
        let intervalOf3Hours: TimeInterval = 3 * 60 * 60
        
        if city != theCurrentTimerCityString {
            if theTimer != nil {
                theTimer?.invalidate()
                print("invalidating old timer")
            }
            theTimer = Timer(fire: Date(), interval: intervalOf3Hours, repeats: true) { (timer) in
                print("3 Hour interval of Updating the Data for \(city)")
                if city.isEmpty != true {
                    self.theCurrentTimerCityString = city
                    self.makeACallToWeatherWithSpinner(city)
                }
            }
            theTimer?.fire()
        }
            
        // MARK: Make a normal
        makeACallToWeatherWithSpinner(city)
    }
    
    func makeACallToWeatherWithSpinner(_ city: String) {
        
        // MARK: Call to Network Code
        
        startSpinnerOnMain()
        makeACallToTheWeather(withCity: city) { (enumResult) in
            
            self.stopSpinnerOnMain()
            switch enumResult {
            case .ok(let theWeatherFound):
                self.currentWeatherForCity = theWeatherFound
            case .error(let theError):
                self.handleErrorWithAnAlert()
                print("The error \(theError)")
            default:
                print("Others")
            }
        }.resume()

        
    }
    
    // MARK : UI
    
    private func startSpinnerOnMain() {
        DispatchQueue.main.async {
            self.theSpinner.startAnimating()
        }
    }
    
    private func stopSpinnerOnMain() {
        DispatchQueue.main.async {
            self.theSpinner.stopAnimating()
        }
    }

    
    func handleErrorWithAnAlert() {
        
        DispatchQueue.main.async {
            
             let alert = UIAlertController(title: "Sorry there was an Problem", message: "Please try again later", preferredStyle: .alert)

             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

             self.present(alert, animated: true)
        }
    }
    
    @IBAction func startSearchingForWeatherInACity(_ sender: UIButton) {
        handleStartSearchingForWeather()
    }
    
    private func handleStartSearchingForWeather()  {
                
        guard let cityText = theCityNameField.text, !cityText.isEmpty  else {
            return
        }
        
        // TODO: Check is This a valid city ?
         let encoded = cityText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        guard let cityString = encoded else {
            handleErrorWithAnAlert()
            theCityNameField.text = ""
            return
        }
        
       // , cityString.isAlphanumeric == true
        
        print("City is: \(cityText) encoded: \(cityString)")
                
        let urlStringMunich = "https://api.openweathermap.org/data/2.5/forecast?q=\(cityString)&appid=822b4b31eb8468e6221ccd8e9623d3dd"
        
        updateTheWeather(cityStringURLToRetrieve: urlStringMunich)

    }
    
    
    
    // MARK: Call to Network  & Utilities code
    
    private func makeACallToTheWeather(withCity city: String = "Munich", completion: ((_ callSuccess: networkCallResults ) -> ())?) -> URLSessionDataTask {
        

        let theURL = URL(string: city)!
        let theSession = URLSession.shared
        let task = theSession.weatherTask(with: theURL) { (weather, response, error) in
            
            guard let closureToRun = completion else {
                return
            }
            
            if let error = error {
                closureToRun(.error(error))
            }
            
            guard let weather = weather else {
                return
            }

            closureToRun(.ok(weather))
        }
        return task
    }
    
    private func setUpCoreLocation()  {
        location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        location.delegate = self
        location.requestWhenInUseAuthorization()
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        theMapView.setRegion(coordinateRegion, animated: true)
        print("\(#function)")
    }
}

extension WeatherViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        handleStartSearchingForWeather()
        return false
    }
}

// MARK: Not Currently Used
extension WeatherViewController: MKMapViewDelegate {
    
}

extension WeatherViewController: UITableViewDataSource {
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return theNumberOfRowsInTableView
    }

    // Provide a cell object for each row.
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? WeatherTableViewCell else { return }

        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        print("TableView indexPath is: \(indexPath.row)")
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }

     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? WeatherTableViewCell else { return }

        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }


    
}

// MARK: Not Currently Used
extension WeatherViewController: UITableViewDelegate {
    
}

// MARK: Not Currently Used but could be used to get lat long coordinates and then weather
extension WeatherViewController:  CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // print("here")
    }
    
    func changeMapToAnotherCiry()  {
        //theMapView.setCenter(_ coordinate: CLLocationCoordinate2D, animated: Bool)
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latestLocation: CLLocation = locations[locations.count - 1]
        self.latestLocation = latestLocation
    }

}

extension WeatherViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theNumberOfCellsInHorizontalCollectionView
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherForDayCollectionCell", for: indexPath) as? HourWeatherCollectionViewCell

        cell?.backgroundColor = collectionViewColors.first
        
        let timeCellSection = collectionView.tag
        if currentWeatherForCity?.list?.count == 40 {
            let increment = timeCellSection * 8
            let actualIndex = indexPath.row + increment
            let dateAndTime = currentWeatherForCity?.list?[actualIndex].dtTxt ?? "00.00"
            let temperature = Double(currentWeatherForCity?.list?[actualIndex].main?.temp ?? 00.00)
            let weatherDescription = currentWeatherForCity?.list?[actualIndex].weather?.first?.main
            cell?.Time.text = dateAndTime
            cell?.Temperature.text = String(format: "%.0f", temperature - 273.15)
            let systemIcon = determineSystemImageForWeather(textToMatch: weatherDescription)
            cell?.Icon.image = UIImage(systemName: systemIcon)
            
            print(" CollectionView Number : \(timeCellSection)  List index : \(actualIndex) icon : \(systemIcon)")
        }

        return cell ?? HourWeatherCollectionViewCell()
    }
    // MARK: String pattern matching to determine System Icon to show
    private func determineSystemImageForWeather(textToMatch match: String?) -> String {
        
        // cloud.fill / sun.max / cloud.rain.fill / snow /
        // Pattern match - string
        
        guard let match = match else {
            return "thermometer"
        }
        
        print("weather description is: \(match)")
        
        if match.contains("Clouds") {
            return "cloud.fill"
        } else if match.contains("Sun") {
            return "sun.max"
        } else if match.contains("Rain") {
            return "cloud.rain.fill"
        } else if match.contains("Snow") {
            return "snow"
        } else if match.contains("Clear") {
            return "rays"
        }
        
        return "thermometer"
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}


// MARK: Utility extensions for Colors

extension UIColor {
    
    class func randomColor() -> UIColor {

        let hue = CGFloat(arc4random() % 100) / 100
        let saturation = CGFloat(arc4random() % 100) / 100
        let brightness = CGFloat(arc4random() % 100) / 100

        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    class func generateRandomPastelColor(withMixedColor mixColor: UIColor?) -> UIColor {
    // Randomly generate number in closure
    let randomColorGenerator = { ()-> CGFloat in
        CGFloat(arc4random() % 256 ) / 256
    }
        
    var red: CGFloat = randomColorGenerator()
    var green: CGFloat = randomColorGenerator()
    var blue: CGFloat = randomColorGenerator()
        
    // Mix the color
    if let mixColor = mixColor {
        var mixRed: CGFloat = 0, mixGreen: CGFloat = 0, mixBlue: CGFloat = 0;
        mixColor.getRed(&mixRed, green: &mixGreen, blue: &mixBlue, alpha: nil)
        
        red = (red + mixRed) / 2;
        green = (green + mixGreen) / 2;
        blue = (blue + mixBlue) / 2;
    }
        
    return UIColor(red: red, green: green, blue: blue, alpha: 1)

    }

}

extension String {
    /// Allows only `a-zA-Z0-9`
    public var isAlphanumeric: Bool {
        guard !isEmpty else {
            return false
        }
        let allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        let characterSet = CharacterSet(charactersIn: allowed)
        guard rangeOfCharacter(from: characterSet.inverted) == nil else {
            return false
        }
        return true
    }
}



