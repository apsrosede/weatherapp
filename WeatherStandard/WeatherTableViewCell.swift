//
//  WeatherTableViewCell.swift
//  WeatherStandard
//
//  Created by AdrianRoseMacBookAir on 12.10.19.
//  Copyright © 2019 apsrose. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension WeatherTableViewCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

class HourWeatherCollectionViewCell: UICollectionViewCell {
    
    // date / time / temperature / icon
    
    @IBOutlet weak var Time: UILabel!
    
    @IBOutlet weak var Temperature: UILabel!
    
    @IBOutlet weak var Icon: UIImageView!
    
}

