WeatherStandard App

The App uses the openweathermap API with city text search to find weather data for 40 time intervals over 5 days.

The App uses Apple's codable ( decodable based upon Structs within Structs ) with optionals to parse the returned data from the Server.

The Attached screen shot from the Simulator shows the UI on an iPhone 8 with iOS 13.

weatherapp/WeatherStandard/Simulator Screen Shot - iPhone 8 - 2019-10-17 at 13.25.09.png

In order to build a User friendly UI Mapkit and a MapView where also added to the Main Screen to give the User visual feedback about which City they are seeing weather about.

The App uses a UITableView which has embedded in each UITableViewCell a horizontal UICollectionView. This means that 2 sets of CallBack delegates need to be handled in order to show the Weather Cells with the Date/Time and Temperature and Visual Icon representing the current weather in the city for those 3 hours.

The latitude and longitude returned from the weather API provide the ability to move the MapView to the correct visual location each time.

The last search results are saved locally on the device each time and if the User goes offline then by moving the slider on the top right this data is loaded as long with the previous MapView.

Xcode 11.0 was used on a MacBook 12" with Catalina 10.15 (Actual Release) in order write build and run the Code.

There was not enough time in the 5 Days to complete the Core Data Model and write code for the NSManagedModels with NSFetchedResultsController for the ViewController.

There are only 2 Unit Tests.





